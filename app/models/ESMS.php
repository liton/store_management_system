<?php

/**
 * Created by PhpStorm.
 * User: Liton ESMS Class
 */
class ESMS
{
    private $cxn;

    function connectToDb()
    {
        require_once 'models/Database.php';
        $vars = "include/vars.php";
        $this->cxn = new Database($vars);
    }

    function close()
    {
        $this->cxn->close();
    }
}