<?php

/**
 * Created by PhpStorm.
 * User: Liton
 * : Add  Class
 */
class Add extends ESMS
{
    private $data;
    private $tablename;
    private $cxn;

    public function __construct($data, $tablename)
    {
        if(is_array($data))
        {
            $this->data      = $data;
            $this->tablename = $tablename;
        }
        else
        {
            throw new Exception("Error: the data must be in an array.");
        }

        $this->connectToDb();

        // isert the data into the table
        $this->AddData();

        $this->close();
    }

    // isert the data into the table
    function AddData()
    {
        foreach ($this->data as $key => $value)
        {
            $keys[]  = $key;
            $values[] = $value;
        }
//        $keys = array_keys($this->data);
//        $values = array_values($this->data);

        $tblKeys    = implode($keys, ",");
        $dataValues = '"'.  implode($values, '","').'"';

        $query = "INSERT INTO $this->tablename ($tblKeys) VALUES ($dataValues)";
//         var_dump($query) or die();
        if($sql = mysql_query($query))
        {
            return TRUE;
        }
        else
        {
            throw new Exception("Error: Can't excute the Add query.");
            return FALSE;
        }
    }

}