<?php
/**
 * Created by PhpStorm.
 * User: Liton
 * Category
 */
?>

<h3>Category</h3>
<h2><a href="?page=category&action=add"> Add New Category </a></h2>
<?php
if ($_POST OR @$_GET['action']) {

    if (isset($_GET['action']) && $_GET['action'] == "add") {

        if (isset($_POST['submit']) && $_POST['submit'] == "Add") {
            //`category` :: `id`, `category`

            $newPage['category'] = $_POST['category'];

           $tabename = "product_category";

            try {
                include 'models/ESMS.php';
                include 'models/Add.php';
                $addNewPage = new Add($newPage, $tabename);
                if ($addNewPage) {
                    echo '<script type="text/javascript"> alert("The New Category was Created !"); history.back();</script>';
                }
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        } else {
            try {
                //$PageDataDisplay;
                include 'models/ESMS.php';
                include 'models/Display.php';
                $tablename = "product_category";
                $dsiSections = new Display($tablename);
                $PageDataDisplay = $dsiSections->getAllData();
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }

            include 'veiws/AddCategory.php';
        }
    }

//    // Delete:
    if (isset($_GET['action']) AND $_GET['action'] == "delete") {

        try {
            include 'models/ESMS.php';
            include 'models/Delete.php';
            $tablename = "product_category";
            $id = $_GET['id'];

            $deSec = new Delete($tablename);
            $deSec->deletRecordByID($id);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

//    //edit
    if (isset($_GET['action']) AND $_GET['action'] == "edit") {

        $id = $_GET['id'];
        $tablename = "product_category";

        include 'models/ESMS.php';
        include 'models/Display.php';

        $displaypagedata = new Display($tablename);
        $pagedata = $displaypagedata->getRecordByID($id);

        $tablename = "product_category";
        $dsiSections = new Display($tablename);
        $PageDataDisplay = $dsiSections->getAllData();

        include 'veiws/editCategory.php';

        if(isset($_POST['submit']) && $_POST['submit'] == "Edit")
        {
            $editPage['category'] = $_POST['category'];
                // must be reviewed

            $tabename = "product_category";
            $id = $_GET['id'];
            try {
                include 'models/Update.php';

                $updatePage  = new Update($editPage, $tabename);
                $updatedPage = $updatePage->editData($id);

                if($updatedPage)
                {
                    echo '<script type="text/javascript"> alert("The New Page was updated !"); history.back();</script>';
                }

            } catch (Exception $exc) {
                echo $exc->getMessage();
            }

        }
    }
    
} else {
    include 'models/ESMS.php';
    include 'models/Display.php';
    $tablename = "product_category";

    $displaypages = new Display($tablename);
    $PagesDataDisplay = $displaypages->getAllData();

    include 'veiws/category.php';
}
?>
